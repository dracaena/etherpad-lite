# A real-time collaborative editor for the web
This is a fork of [etherpad-lite](https://github.com/ether/etherpad-lite).

## About
Etherpad is a real-time collaborative editor [scalable to thousands of simultaneous real time users](http://scale.etherpad.org/). It provides [full data export](https://github.com/ether/etherpad-lite/wiki/Understanding-Etherpad's-Full-Data-Export-capabilities) capabilities, and runs on _your_ server, under _your_ control.

# Install

### Clone the repo

    apt install git
    cd /opt/
    git clone https://github.com/ether/etherpad-lite.git

## Add plugins
i.e. edit Dockerfile to add plugin and LibreOffice   

    ARG ETHERPAD_PLUGINS="ep_align ep_comments_page ep_delete_after_delay ep_font_color ep_headings2 ep_webpack"
    ARG INSTALL_SOFFICE=true

## Setup 
* docker-compose.yml file         

    `wget https://framagit.org/_dracaena/etherpad-lite/-/blob/master/docker-compose.yml`

* env variables             

    `cp .env.example .env`

* Apikey         

    `echo "secret" > APIKEY.txt`

## Build    
    docker build . -t tagname/etherpad-lite

## Start

    docker-compose up -d


# Nginx

example configuration for Nginx

    server {
        server_name pad.example.com; # managed by Certbot
        # return 404; # managed by Certbot

        location / {
            proxy_pass http://127.0.0.1:17201/;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_redirect default;
      }

        listen [::]:443 ssl; # managed by Certbot
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/pad.example.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/pad.example.com/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    }

    server {
        if ($host = pad.example.com) {
            return 301 https://$host$request_uri;
        } # managed by Certbot

      listen 80 ;
      listen [::]:80 ;
        server_name pad.example.com;
        return 404; # managed by Certbot

    }
